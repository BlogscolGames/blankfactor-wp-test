<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Hall_Of_Justice
 * @subpackage Hall_Of_Justice/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Hall_Of_Justice
 * @subpackage Hall_Of_Justice/includes
 * @author     Miguel Mariano <miguel@blogscol.com>
 */
class Hall_Of_Justice_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wppb.me/
 * @since             1.0.0
 * @package           Hall_Of_Justice
 *
 * @wordpress-plugin
 * Plugin Name:       Hall of Justice
 * Plugin URI:        https://wppb.me/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Miguel Mariano
 * Author URI:        https://wppb.me/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       hall-of-justice
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'HALL_OF_JUSTICE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-hall-of-justice-activator.php
 */
function activate_hall_of_justice()
{
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hall-of-justice-activator.php';
	Hall_Of_Justice_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-hall-of-justice-deactivator.php
 */
function deactivate_hall_of_justice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hall-of-justice-deactivator.php';
	Hall_Of_Justice_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_hall_of_justice' );
register_deactivation_hook( __FILE__, 'deactivate_hall_of_justice' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-hall-of-justice.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_hall_of_justice()
{
	$plugin = new Hall_Of_Justice();
	$plugin->run();

	require_once plugin_dir_path( __FILE__ ) . 'admin_functions.php';

	Admin_Functions::init();
}
run_hall_of_justice();
<?php
class View_Functions
{
	public static function list_characters_shortcode($content, $data)
	{
		$type = $data['type'];

		if($type == "heroes")
			$type = "hero";

		if($type == "villains")
			$type = "villain";

		if($type == "all")
			$type = array('hero', 'villain');

		$posts = get_posts([
			'post_type' 	=> 'characters',
			'post_status'	=> 'publish',
			'numberposts'	=> $data['size'],
			'meta_key'		=> 'type',
			'meta_value'	=> $type,
			'fields'		=> 'ids'
		]);

		ob_start();
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12"><p class="font-weight-bold pt-2 mb-1 text-center"><?php echo $content; ?></p></div>

				<div class="col-md-12">
					<div class="row no-gutters">
					<?php
						echo '<div class="list-group">';
						foreach($posts as $post)
						{
							self::list_character_view($post);
						}
						echo '</div>';
					?>
					</div>
				</div>
				<div class="col-md-12">
					<a class="font-weight-bold btn btn-primary btn-lg btn-block" id="view_more_characters" href="<?php echo get_post_type_archive_link('characters'); ?>" role="button">View all Characters</a>
				</div>
			</div>
		</div>
		<?php
		return ob_get_clean();
	}

	public static function card_character_view($postID)
	{
		$type = get_post_meta($postID, 'type', true);
		?>
		<div class="col-md-3 charactersDC <?php echo $type; ?>">
  			<a href="<?php echo get_the_permalink($postID); ?>" class="list-group-item list-group-item-action">		
				<div class="card">			
				  <?php echo get_the_post_thumbnail($postID, 'full', array( 'class' => 'card-img-top' )); ?>
					<div class="card-body">
						<p class="font-weight-bold pt-2 mb-1 text-center titleArchive"><?php echo get_the_title($postID); ?></p>
				  	</div>
				</div>
			</a>
		</div>
		<?php
	}

	public static function list_character_view($postID)
	{
		?>
        <div class="list-group-item">
  			<a href="<?php echo get_the_permalink($postID); ?>" class="list-group-item list-group-item-action">
	            <div class="row">
	                <div class="col-md-12 col-12">
	                    <div class="row">
	                        <div class="col-md-4 user-img pt-1">
							  <?php echo get_the_post_thumbnail($postID, 'thumbnail', array( 'class' => 'img-responsive card-img-top' )); ?>
	                        </div>
	                        <div class="col-md-8">
	                            <p class="font-weight-bold pt-2 mb-1 text-center text-md-left listText"><?php echo get_the_title($postID); ?></p>
	                            <div class="user-detail">
	                                <p class="m-0"><?php echo self::excerpt(get_post_meta($postID, 'bio', true), 100); ?></p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
  			</a>
        </div>
		<?php
	}

	public static function single_character_view($postID)
	{
		$imageID = get_post_meta($postID, 'second_featured_img', true);

		if($imageID)
		{
			$singleRow = "singleRowIMG";
			$singleInfo = "singleInfoIMG";

			$image_attributes = wp_get_attachment_image_src( $imageID, "full" )
			?>
				<div class="col-md-12 col-12 imageFull">
					<?php echo '<img class="img-responsive card-img-top" src="' . $image_attributes[0] . '">'; ?>
				</div>
			<?php			
		}
		else
		{
			$singleRow = "singleRow";
			$singleInfo = "singleInfo";
		}

		?>
            <div class="col-md-12 col-12 backgroundFill">
                <div class="row <?php echo $singleRow;?>">
                    <div class="col-md-3">
                    </div>

                    <div class="col-md-1 user-img pt-1">
					  <?php echo get_the_post_thumbnail($postID, 'medium', array( 'class' => ' card-img-top' )); ?>
                    </div>
                    <div class="col-md-8">
                        <p class="font-weight-bold pt-2 mb-1 text-center text-capitalize text-md-left singleTitleText"><?php echo get_the_title($postID); ?></p>

                        <p class="font-weight-bold pt-2 mb-1 text-center text-capitalize text-md-left singleTitleText"><?php echo get_post_meta($postID, 'type', true); ?></p
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-12 backgroundFill">
                <div class="row">            
	            	<div class="col-md-3">
	            	</div>

	            	<div class="<?php echo $singleInfo;?> col-md-6">
                        <p class="font-weight-bold pt-2 mb-1 text-capitalize">Biography</p>
	            		<p><?php echo get_post_meta($postID, 'bio', true); ?></p>
                        
                        <p class="font-weight-bold pt-2 mb-1 text-capitalize">Last Action</p>
	            		<p><?php echo get_post_meta($postID, 'lastAction', true); ?></p>
                        
                        <p class="font-weight-bold pt-2 mb-1 text-capitalize">Source Of Their Powers</p>
	            		<p><?php echo get_post_meta($postID, 'sourcePowers', true); ?></p>

                        <p class="font-weight-bold pt-2 mb-1 text-capitalize">Weaknesses</p>
	            		<p><?php echo get_post_meta($postID, 'weaknesses', true); ?></p>
	            	</div>

	            	<div class="col-md-3">
	            	</div>
	            </div>
			</div>

		<?php
	}


	public static function excerpt($title, $cutOffLength)
	{
	    $charAtPosition = "";
	    $titleLength = strlen($title);

	    do
	    {
	        $cutOffLength++;
	        $charAtPosition = substr($title, $cutOffLength, 1);
	    } while ($cutOffLength < $titleLength && $charAtPosition != " ");

	    return substr($title, 0, $cutOffLength) . '...';
	}
}
?>
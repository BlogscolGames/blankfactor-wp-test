<?php
get_header();

require_once plugin_dir_path( __FILE__ ) . 'views.php';
?>

<main id="site-content" role="main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<p class="font-weight-bold pt-2 mb-1 text-center"><?php echo post_type_archive_title(); ?></p>
			</div>

		    <div class="col-md-12">
				<div class="row no-gutters">
					<div class="col-md-4 col-4">
				    	<p class="font-weight-bold pt-2 mb-1 text-center">Filter</p>
				    </div>
				    <div class="col-md-4 col-4">
						<select name="type" id="filter">
							<option value="hero">Hero</option> 
							<option value="villain">Villain</option>
							<option value="all" selected>All</option>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="row no-gutters">
					<?php
					if ( have_posts() )
					{
						while ( have_posts() )
						{
							the_post();
							View_Functions::card_character_view(get_the_ID());
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
</main><!-- #site-content -->

<?php get_footer(); ?>

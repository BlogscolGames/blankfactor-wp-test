<?php
class Admin_Functions {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function init()
	{
		add_action( 'init', 'register_hall_of_justice_posts' );
		add_shortcode('characters', 'hall_of_justice_att');
	}
}

function hall_of_justice_att($atts, $content = null)
{
    $default = array
    (
        'type' => 'all',
        'size' => 5,
    );

    $characters = shortcode_atts($default, $atts);
    $content = do_shortcode($content);

	require_once plugin_dir_path( __FILE__ ) . 'views.php';

	return View_Functions::list_characters_shortcode($content, $characters);
}

function register_hall_of_justice_posts()
{
	//	require_once plugin_dir_path( __FILE__ ) . 'includes/class-hall-of-justice-activator.php';
    $labels = array
    (
        'name'                => _x( 'Characters', 'Post Type General Name', 'hall-of-justice' ),
        'singular_name'       => _x( 'Character', 'Post Type Singular Name', 'hall-of-justice' ),
        'menu_name'           => __( 'Characters', 'hall-of-justice' ),
        'parent_item_colon'   => __( 'Parent Character', 'hall-of-justice' ),
        'all_items'           => __( 'All Characters', 'hall-of-justice' ),
        'view_item'           => __( 'View Character', 'hall-of-justice' ),
        'add_new_item'        => __( 'Add New Character', 'hall-of-justice' ),
        'add_new'             => __( 'Add New', 'hall-of-justice' ),
        'edit_item'           => __( 'Edit Character', 'hall-of-justice' ),
        'update_item'         => __( 'Update Character', 'hall-of-justice' ),
        'search_items'        => __( 'Search Character', 'hall-of-justice' ),
        'not_found'           => __( 'Not Found', 'hall-of-justice' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'hall-of-justice' ),
    );
      
	$args = array
	(
		'labels'             => $labels,
		'description'        => __( 'DC Characters', 'hall-of-justice' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'characters' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 4,
    	'menu_icon'           => plugins_url() . '/hall-of-justice/admin/icons/heroes.png',
		'supports'           => array( 'title', 'thumbnail' )
	);

	register_post_type( 'characters', $args );
	flush_rewrite_rules();
}

add_action( 'add_meta_boxes', 'dc_meta_box_add' );
function dc_meta_box_add()
{
  add_meta_box( 'dc-meta-box', 'Data', 'dc_meta_box_cb', ['characters'], 'normal', 'high' );
}

function dc_meta_box_cb( $post )
{
	$values = get_post_custom( $post->ID );

	$type = isset( $values['type'] ) ? esc_attr( $values['type'][0] ) : '';

	$bio = isset( $values['bio'] ) ? esc_attr( $values['bio'][0] ) : '';
	$lastAction = isset( $values['lastAction'] ) ? esc_attr( $values['lastAction'][0] ) : '';
	$sourcePowers = isset( $values['sourcePowers'] ) ? esc_attr( $values['sourcePowers'][0] ) : '';
	$weaknesses = isset( $values['weaknesses'] ) ? esc_attr( $values['weaknesses'][0] ) : '';

    ?>
    <div class="">
		<p>
	    	<label for="backgroundSingle"><h2><span style="font-size: 20px;">Background image</span></h2></label>
	    </p>
	    <p>
	    <?php
		    $meta_key = 'second_featured_img';

	    	echo dc_image_uploader_field($meta_key, get_post_meta($post->ID, $meta_key, true) ); ?>
		</p>
	</div>
    
    <div class="">
		<p>
	    	<label for="type"><h2><span style="font-size: 20px;">Type of Character
</span></h2></label>
	    </p>
	    <p>
			<select name="type">
				<option value="hero" <?php selected( $type, 'hero' ); ?>>Hero</option> 
				<option value="villain" <?php selected( $type, 'villain' ); ?>>Villain</option>
			</select>
		</p>
	</div>    

    <div class="">
		<p>
	    	<label for="bio"><h2><span style="font-size: 20px;">Biography</span></h2></label>
	    </p>
	    <p>
			<?php 
	        	wp_editor( htmlspecialchars_decode($bio), 'bio', $settings = array('textarea_name'=>'bio', 'media_buttons' => false, 'teeny' => true, 'textarea_rows' => 10) );
 			?>
		</p>
	</div>

    <div class="">
		<p>
	    	<label for="lastAction"><h2><span style="font-size: 20px;">Last Action</span></h2></label>
	    </p>
	    <p>
			<?php 
	        	wp_editor( htmlspecialchars_decode($lastAction), 'lastAction', $settings = array('textarea_name'=>'lastAction', 'teeny' => true, 'textarea_rows' => 10) );
 			?>
		</p>
	</div>

    <div class="">
		<p>
	    	<label for="sourcePowers"><h2><span style="font-size: 20px;">Source of their Powers</span></h2></label>
	    </p>
	    <p>
			<?php 
	        	wp_editor( htmlspecialchars_decode($sourcePowers), 'sourcePowers', $settings = array('textarea_name'=>'sourcePowers','media_buttons' => false, 'teeny' => true, 'textarea_rows' => 10) );
 			?>
		</p>
	</div>

    <div class="">
		<p>
	    	<label for="weaknesses"><h2><span style="font-size: 20px;">Weaknesses</span></h2></label>
	    </p>
		<p>
	    	<label for="weaknesses"><h2>Insert Weaknesses separated by commas: <b>weaknesses1</b>, <b>weaknesses2</b></h2></label>
	    </p>
	    <p>
	    	<input type="text" name="weaknesses" id="weaknesses" value="<?php echo $weaknesses; ?>"/>
		</p>
	</div>
	<?php
}

function dc_image_uploader_field( $name, $value = '')
{
    $image = ' button">Upload image';
    $image_size = 'medium';
    $display = 'none';

    if( $image_attributes = wp_get_attachment_image_src( $value, $image_size ) )
    {
        $image = '"><img src="' . $image_attributes[0] . '" style="max-width:95%;display:block;" />';
        $display = 'inline-block';
    } 

    return '
    <div>
        <a href="#" class="dc_upload_image_button' . $image . '</a>
        <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
        <a href="#" class="dc_remove_image_button" style="display:inline-block;display:' . $display . '">Remove image</a>
    </div>';
}

add_action( 'save_post', 'dc_meta_box_save' );
function dc_meta_box_save( $post_id )
{
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    if( isset( $_POST['type'] ) )
        update_post_meta( $post_id, 'type', $_POST['type'] );

    if( isset( $_POST['bio'] ) )
        update_post_meta( $post_id, 'bio', $_POST['bio'] );

    if( isset( $_POST['lastAction'] ) )
        update_post_meta( $post_id, 'lastAction', $_POST['lastAction'] );

    if( isset( $_POST['sourcePowers'] ) )
        update_post_meta( $post_id, 'sourcePowers', $_POST['sourcePowers'] );

    if( isset( $_POST['weaknesses'] ) )
        update_post_meta( $post_id, 'weaknesses', $_POST['weaknesses'] );

    if( isset( $_POST['second_featured_img'] ) )
    	update_post_meta( $post_id, 'second_featured_img', $_POST['second_featured_img'] );  
}

add_filter('template_include', 'characters_template');
function characters_template( $template )
{
	if ( is_post_type_archive('characters') )
  	{
		return plugin_dir_path(__FILE__) . 'archive-characters.php';
	}
	else if( is_singular('characters') )
	{
		return plugin_dir_path(__FILE__) . 'single-characters.php';
	}

	return $template;
}
?>
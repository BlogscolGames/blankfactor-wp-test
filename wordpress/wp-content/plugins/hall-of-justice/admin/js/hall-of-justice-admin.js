(function( $ ) {
	'use strict';



})( jQuery );

jQuery(document).ready(function($)
{
	$('#weaknesses').inputTags(
	{
		max: 5,
        maxLength: 100,
        minLength: 3,
		errors:
		{
			empty: 'Tenga en cuenta que no se puede agregar una etiqueta vacía.',
			minLength: 'Tenga en cuenta que su etiqueta debe tener al menos %s caracteres',
			maxLength: 'Tenga en cuenta que su etiqueta no debe exceder %s caracteres',
			max: 'Tenga en cuenta que el número de etiquetas no debe exceder %s.',
			email: "Tenga en cuenta que la dirección de correo electrónico que ingresó no es válida",
			exists: '¡Advertencia, esta etiqueta ya existe!',
			autocomplete_only: 'Tenga en cuenta que debe seleccionar un valor de la lista',
			timeout: 8000
		}
	});	

    $('body').on('click', '.dc_upload_image_button', function(e)
    {
        e.preventDefault();

		var button = $(this),custom_uploader = wp.media(
		{
            title: 'Insert image',
            library :
            {
                type : 'image'
            },
            button:
            {
                text: 'Use this image'
            },
            multiple: false
        }).on('select', function()
        {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $(button).removeClass('button').html('<img class="true_pre_image" src="' + attachment.url + '" style="max-width:95%;display:block;" />').next().val(attachment.id).next().show();
        })
        .open();
    });

    $('body').on('click', '.dc_remove_image_button', function()
    {
        $(this).hide().prev().val('').prev().addClass('button').html('Upload image');
        return false;
    });	
});
